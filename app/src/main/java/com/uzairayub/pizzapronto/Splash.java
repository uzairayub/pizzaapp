package com.uzairayub.pizzapronto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.Utilities;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(1000);
                    if(Utilities.getInstance(Splash.this).getBooleanPreferences(Constants.LOGIN))
                    {
                        startActivity(new Intent(Splash.this, MainActivity.class));
                    }
                    else
                    {
                        startActivity(new Intent(Splash.this, Register.class));
                    }
                    finish();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }
}
