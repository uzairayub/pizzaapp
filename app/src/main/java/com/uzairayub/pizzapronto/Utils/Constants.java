package com.uzairayub.pizzapronto.Utils;

public interface Constants {
    String CHEESE = "cheese";
    String MEAT = "meat";
    String SAUCE = "sauce";
    String VEGETABLES = "vegetable";

    String CART_LIST = "cartList";

    String EMAIL = "email";
    String NAME = "name";
    String LOGIN = "login";
}
