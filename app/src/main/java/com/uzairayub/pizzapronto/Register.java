package com.uzairayub.pizzapronto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.Utilities;

public class Register extends AppCompatActivity implements View.OnClickListener {

    private EditText etName, etEmail;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initial();
    }

    private void initial()
    {
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(etName.getText().toString().isEmpty())
        {
            etName.setError("Name is required");
            return;
        }
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.setError("Email is required");
            return;
        }
        Utilities.getInstance(Register.this).saveStringPreferences(Constants.EMAIL, etEmail.getText().toString());
        Utilities.getInstance(Register.this).saveStringPreferences(Constants.NAME, etName.getText().toString());
        Utilities.getInstance(Register.this).saveBooleanPreferences(Constants.LOGIN, true);
        startActivity(new Intent(Register.this, MainActivity.class));
        finish();

    }
}
