package com.uzairayub.pizzapronto.PizzaConstructor.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;

import java.util.ArrayList;

public class AdapterMaterial extends RecyclerView.Adapter<AdapterMaterial.MyViewHolder> {
    private ArrayList<ProductModel> list;

    public AdapterMaterial(ArrayList<ProductModel> list) {
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == (list.size()-1))
        {
            return 0;
        }
        return 1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 1)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_material, parent, false);
            return new MyViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_space, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        if(getItemViewType(position) == 1)
        {
            holder.ivImage.setImageResource(list.get(position).getImage());
            holder.tvMaterial.setText(list.get(position).getName());
            holder.tvCount.setText(list.get(position).getCount()+"");

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = list.get(position).getCount();
                    count = count+1;
                    list.get(position).setCount(count);
                    notifyDataSetChanged();
                }
            });

            holder.btnLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = list.get(position).getCount();
                    if(count > 0)
                    {
                        count = count-1;
                        list.get(position).setCount(count);
                    }
                    notifyDataSetChanged();
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivImage;
        TextView tvMaterial, tvCount;
        ImageButton btnLess, btnAdd;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            tvMaterial = itemView.findViewById(R.id.tvMaterial);
            tvCount = itemView.findViewById(R.id.tvCount);
            btnLess = itemView.findViewById(R.id.btnLess);
            btnAdd = itemView.findViewById(R.id.btnAdd);
        }
    }
}
