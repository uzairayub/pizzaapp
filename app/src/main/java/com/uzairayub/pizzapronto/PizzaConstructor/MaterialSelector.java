package com.uzairayub.pizzapronto.PizzaConstructor;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.uzairayub.pizzapronto.PizzaConstructor.Adapters.AdapterMaterial;
import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;
import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.SnappyDBUtil;

import java.util.ArrayList;

public class MaterialSelector extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvMaterials;
    private int type;
    private ArrayList<ProductModel> list;
    private FloatingActionButton btnDone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_selector);

        initial();
    }

    private void initial()
    {
        btnDone = findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
        type = getIntent().getExtras().getInt("type");
        rvMaterials = findViewById(R.id.rvMaterials);
        rvMaterials.setLayoutManager(new LinearLayoutManager(MaterialSelector.this));
        if(type == 1)
        {
            list = populateCheeseList();
            rvMaterials.setAdapter(new AdapterMaterial(list));
        }
        else if(type == 2)
        {
            list = populateMeatList();
            rvMaterials.setAdapter(new AdapterMaterial(list));
        }
        else if(type == 3)
        {
            list = populateSauceList();
            rvMaterials.setAdapter(new AdapterMaterial(list));
        }
        else if(type == 4)
        {
            list = populateVegetableList();
            rvMaterials.setAdapter(new AdapterMaterial(list));
        }


    }

    private ArrayList<ProductModel> populateCheeseList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;

        model = new ProductModel();
        model.setImage(R.drawable.cheese_1);
        model.setName("Bergader");
        model.setCount(0);
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.cheese_2);
        model.setName("Cheder");
        model.setCount(0);
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.cheese_3);
        model.setName("Feta");
        model.setCount(0);
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.cheese_4);
        model.setName("Permesan");
        model.setCount(0);
        list.add(model);

        return list;
    }

    private ArrayList<ProductModel> populateMeatList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;

        model = new ProductModel();
        model.setImage(R.drawable.meat_1);
        model.setName("Bacon");
        model.setCount(0);
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.meat_2);
        model.setCount(0);
        model.setName("Chicken");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.meat_3);
        model.setCount(0);
        model.setName("Ham");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.meat_4);
        model.setCount(0);
        model.setName("una");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.meat_5);
        model.setCount(0);
        model.setName("White Sausage");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.meat_6);
        model.setCount(0);
        model.setName("Meat Balls");
        list.add(model);

        return list;
    }

    private ArrayList<ProductModel> populateSauceList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;

        model = new ProductModel();
        model.setImage(R.drawable.sauce_1);
        model.setName("Alfredo Sauce");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.sauce_2);
        model.setName("BBQ Sauce");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.sauce_3);
        model.setName("Pronto Special");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.sauce_4);
        model.setName("Garlic Sauce");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.sauce_5);
        model.setName("Mustered Sauce");
        list.add(model);

        return list;
    }

    private ArrayList<ProductModel> populateVegetableList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;
        model = new ProductModel();
        model.setImage(R.drawable.veg_1);
        model.setName("Cherry Tomato");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_2);
        model.setName("Corn");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_3);
        model.setName("Capsicum");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_4);
        model.setName("Tomato");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_5);
        model.setName("Onion");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_6);
        model.setName("Pickled Cucumber");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_7);
        model.setName("Pineapple");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.veg_8);
        model.setName("Spinach");
        list.add(model);


        return list;
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == btnDone.getId())
        {
            if(type == 1)
            {
                SnappyDBUtil.saveList(Constants.CHEESE, list);
            }
            else if(type == 2)
            {
                SnappyDBUtil.saveList(Constants.MEAT, list);
            }
            else if(type == 3)
            {
                SnappyDBUtil.saveList(Constants.SAUCE, list);
            }
            else if(type == 4)
            {
                SnappyDBUtil.saveList(Constants.VEGETABLES, list);
            }
            finish();
        }
    }
}
