package com.uzairayub.pizzapronto.PizzaConstructor;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;
import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.SnappyDBUtil;

import java.util.ArrayList;

public class ConstructorFragment extends Fragment implements View.OnClickListener {

    private CardView cvCheese, cvMeat, cvSauces, cvVegetables;
    private View view;
    private Button btnAddCart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_constructor, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        btnAddCart = view.findViewById(R.id.btnAddCart);
        btnAddCart.setOnClickListener(this);
        cvCheese = view.findViewById(R.id.cvCheese);
        cvCheese.setOnClickListener(this);
        cvMeat = view.findViewById(R.id.cvMeat);
        cvMeat.setOnClickListener(this);
        cvSauces = view.findViewById(R.id.cvSauces);
        cvSauces.setOnClickListener(this);
        cvVegetables = view.findViewById(R.id.cvVegetables);
        cvVegetables.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.cvCheese:
            {
                startMaterialSelector(1);
                break;
            }
            case R.id.cvMeat:
            {
                startMaterialSelector(2);
                break;
            }
            case R.id.cvSauces:
            {
                startMaterialSelector(3);
                break;
            }
            case R.id.cvVegetables:
            {
                startMaterialSelector(4);
                break;
            }
            case R.id.btnAddCart:
            {
                showDialog();
                break;
            }
        }
    }

    void showDialog() {
        final Dialog dialog = new Dialog(getContext(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_size);

        ViewGroup llSmall, llMedium, llLarge;
        llSmall = dialog.findViewById(R.id.llSmall);
        llSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = new ProductModel();
                item.setPrice("499");
                item.setName("Custom Pizza");
                item.setDescription(getDescription());
                addToCart(item);
            }
        });
        llMedium = dialog.findViewById(R.id.llMedium);
        llMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = new ProductModel();
                item.setPrice("999");
                item.setName("Custom Pizza");
                item.setDescription(getDescription());
                addToCart(item);
            }
        });
        llLarge = dialog.findViewById(R.id.llLarge);
        llLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = new ProductModel();
                item.setPrice("1399");
                item.setName("Custom Pizza");
                item.setDescription(getDescription());
                addToCart(item);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private String getDescription()
    {
        String description = "";
        ArrayList<ProductModel> myList = SnappyDBUtil.getList(Constants.CHEESE, ArrayList.class);
        if(myList != null)
        {
            processList(myList);
        }
        myList = SnappyDBUtil.getList(Constants.MEAT, ArrayList.class);
        if(myList != null)
        {
            processList(myList);
        }
        myList = SnappyDBUtil.getList(Constants.SAUCE, ArrayList.class);
        if(myList != null)
        {
            processList(myList);
        }
        myList = SnappyDBUtil.getList(Constants.VEGETABLES, ArrayList.class);
        if(myList != null)
        {
            processList(myList);
        }
        return description;
    }
    private String processList(ArrayList<ProductModel> myList)
    {
        String description = "";
        for(int i = 0; i < myList.size(); i++)
        {
            if(myList.get(i).getCount() > 0)
            {
                description = description+myList.get(i).getName()+",";
            }
        }
        return description;
    }

    private void addToCart(ProductModel item)
    {
        ArrayList<ProductModel> myList = SnappyDBUtil.getList(Constants.CART_LIST, ArrayList.class);
        if(myList == null)
        {
            myList = new ArrayList<>();
        }
        myList.add(item);
        SnappyDBUtil.saveList(Constants.CART_LIST, myList);
    }
    private void startMaterialSelector(int type)
    {
        Intent intent = new Intent(getContext(), MaterialSelector.class);
        intent.putExtra("type", type);
        startActivity(intent);
    }
}
