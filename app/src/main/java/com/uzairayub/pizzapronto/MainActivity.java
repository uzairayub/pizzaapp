package com.uzairayub.pizzapronto;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.uzairayub.pizzapronto.Menu.MenuFragment;
import com.uzairayub.pizzapronto.PizzaConstructor.ConstructorFragment;
import com.uzairayub.pizzapronto.Products.Fragments.FragmentPizza;
import com.uzairayub.pizzapronto.Products.ProductsFragment;
import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.SnappyDBUtil;

public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId())
            {
                case R.id.nav_products:
                {
                    setFragment(new ProductsFragment());
                    return true;
                }
                case R.id.nav_pizza_maker:
                {
                    setFragment(new ConstructorFragment());
                    return true;
                }
                case R.id.nav_menu:
                {
                    setFragment(new MenuFragment());
                    return true;
                }
            }
            return false;
        }
    };

    private void setFragment(Fragment fragment)
    {
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
//         container
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        setFragment(new ProductsFragment());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SnappyDBUtil.deleteObjectOrList(Constants.CART_LIST);
    }
}
