package com.uzairayub.pizzapronto;

import android.app.Application;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;


public class AppController extends Application {
    private static AppController mInstance;
    private static DB snappy = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public static synchronized DB getSnappyInstance() {
        try {
            if (snappy == null) {
                snappy = DBFactory.open(mInstance);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return snappy;
    }

}
