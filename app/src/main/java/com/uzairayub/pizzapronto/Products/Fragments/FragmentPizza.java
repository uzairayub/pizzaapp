package com.uzairayub.pizzapronto.Products.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.uzairayub.pizzapronto.Products.Adapters.AdapterPizza;
import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;
import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.SnappyDBUtil;

import java.util.ArrayList;
import java.util.List;

public class FragmentPizza extends Fragment implements AdapterPizza.PizzaCallback {

    private ArrayList<ProductModel> list;
    private View view;
    private RecyclerView rvPizza;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_pizza, container, false);
        initial();
        return view;
    }

    private void initial() {
        rvPizza = view.findViewById(R.id.rvPizza);
        rvPizza.setLayoutManager(new LinearLayoutManager(getContext()));
        list = populateList();
        rvPizza.setAdapter(new AdapterPizza(list, this));
    }


    private ArrayList<ProductModel> populateList() {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model = new ProductModel();

        model.setImage(R.drawable.pizza_1);
        model.setName("BBQ Pizza");
        model.setDescription("Bacoon, BBQ sauce, Chicken, Mozarella, Mushrooms, Onion");
        model.setPrice("from 499 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.pizza_2);
        model.setName("Pizza Fajita");
        model.setDescription("Chicken, Olives, Mushrooms, Tomatoes, Mozarella");
        model.setPrice("from 499 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.pizza_3);
        model.setName("Chicken Tikka");
        model.setDescription("Chicken Tikka, Tomatoes, Mozarella, Chadder, White Sauce, Onions");
        model.setPrice("from 499 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.pizza_4);
        model.setName("Chicken Sicillian");
        model.setDescription("Chicken Pepperoni,Jalapenos, Alfredo Sauce, chadder, Mozarella");
        model.setPrice("from 499 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.pizza_5);
        model.setName("Pizza Margarita");
        model.setDescription("Pronto Special Sauce, Mozarella");
        model.setPrice("from 499 RS");
        list.add(model);


        model = new ProductModel();
        model.setImage(R.drawable.pizza_6);
        model.setName("Cheese Pizza");
        model.setDescription("Chicken, Black Olives, Green Olives, Mushrooms, Tomatoes, Mozeralla, Chadder, Jalapenos, Parmesan");
        model.setPrice("from 499 RS");
        list.add(model);


        model = new ProductModel();
        model.setImage(R.drawable.pizza_7);
        model.setName("Pizza Texas");
        model.setDescription("Pronto Special Sauce, Mozarella, Tomatoes, Olives, Chicken");
        model.setPrice("from 499 RS");
        list.add(model);

        return list;

    }


    void showDialog(final int position) {
        final Dialog dialog = new Dialog(getContext(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_size);

        ViewGroup llSmall, llMedium, llLarge;
        llSmall = dialog.findViewById(R.id.llSmall);
        llSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = list.get(position);
                item.setPrice("499");
                addToCart(item);
            }
        });
        llMedium = dialog.findViewById(R.id.llMedium);
        llMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = list.get(position);
                item.setPrice("999");
                addToCart(item);
            }
        });
        llLarge = dialog.findViewById(R.id.llLarge);
        llLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ProductModel item = list.get(position);
                item.setPrice("1399");
                addToCart(item);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    private void addToCart(ProductModel item)
    {
        ArrayList<ProductModel> myList = SnappyDBUtil.getList(Constants.CART_LIST, ArrayList.class);
        if(myList == null)
        {
            myList = new ArrayList<>();
        }
        myList.add(item);
        SnappyDBUtil.saveList(Constants.CART_LIST, myList);
    }
    @Override
    public void onItemClicked(int position) {
        showDialog(position);
    }
}
