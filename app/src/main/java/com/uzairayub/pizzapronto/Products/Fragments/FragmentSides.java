package com.uzairayub.pizzapronto.Products.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.uzairayub.pizzapronto.Products.Adapters.AdapterSides;
import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;

import java.util.ArrayList;

public class FragmentSides extends Fragment {

    private View view;
    private RecyclerView rvSides;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sides, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        rvSides = view.findViewById(R.id.rvSides);
        rvSides.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSides.setAdapter(new AdapterSides(populateList()));
    }


    private ArrayList<ProductModel> populateList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;

        model = new ProductModel();
        model.setImage(R.drawable.side_1);
        model.setName("BBQ Wings");
        model.setPrice("from 299 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_2);
        model.setName("Bread with Bavarian");
        model.setPrice("from 249 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_3);
        model.setName("Bread with Pepperoni");
        model.setPrice("from 299 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_4);
        model.setName("Chicken Strips");
        model.setPrice("from 299 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_5);
        model.setName("Parmesian Bites");
        model.setPrice("from 399 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_6);
        model.setName("Potato Wedeges");
        model.setPrice("from 199 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_7);
        model.setName("Spicy Wings");
        model.setPrice("from 299 RS");
        list.add(model);

        model = new ProductModel();
        model.setImage(R.drawable.side_8);
        model.setName("Tuna Salad");
        model.setPrice("from 299 RS");
        list.add(model);

        return list;
    }
}
