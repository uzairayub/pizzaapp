package com.uzairayub.pizzapronto.Products.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;

import java.util.ArrayList;

public class AdapterPizza extends RecyclerView.Adapter<AdapterPizza.MyViewHolder> {

    private ArrayList<ProductModel> list;
    private PizzaCallback callback;

    public AdapterPizza(ArrayList<ProductModel> list, PizzaCallback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pizza, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position)
    {
        holder.itemImage.setImageResource(list.get(position).getImage());
        holder.itemDescription.setText(list.get(position).getDescription());
        holder.itemName.setText(list.get(position).getName());
        holder.itemPrice.setText(list.get(position).getPrice());
        holder.btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemDescription, itemName, itemPrice;
        Button btnAddCart;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            btnAddCart = itemView.findViewById(R.id.btnAddCart);
            itemImage = itemView.findViewById(R.id.itemImage);
            itemDescription = itemView.findViewById(R.id.itemDescription);
            itemName = itemView.findViewById(R.id.itemName);
            itemPrice = itemView.findViewById(R.id.itemPrice);
        }
    }

    public interface PizzaCallback
    {
        void onItemClicked(int position);
    }
}
