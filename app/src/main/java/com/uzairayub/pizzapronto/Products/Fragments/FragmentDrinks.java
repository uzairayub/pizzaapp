package com.uzairayub.pizzapronto.Products.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.uzairayub.pizzapronto.Products.Adapters.AdapterSides;
import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;

import java.util.ArrayList;

public class FragmentDrinks extends Fragment {

    private View view;
    private RecyclerView rvSides;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sides, container, false);
        initial();
        return view;
    }

    private void initial()
    {
        rvSides = view.findViewById(R.id.rvSides);
        rvSides.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSides.setAdapter(new AdapterSides(populateList()));
    }


    private ArrayList<ProductModel> populateList()
    {
        ArrayList<ProductModel> list = new ArrayList<>();
        ProductModel model;


        model = new ProductModel();
        model.setImage(R.drawable.drink_1);
        model.setName("Nestle");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_2);
        model.setName("Aquafina");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_3);
        model.setName("Dasani");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_4);
        model.setName("Kinley");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_5);
        model.setName("7up");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_6);
        model.setName("Coke");
        model.setPrice("from 100 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_7);
        model.setName("Dew");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_8);
        model.setName("Fanta");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_9);
        model.setName("Mirinda");
        model.setPrice("from 50 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_10);
        model.setName("Nestle Juice");
        model.setPrice("from 150 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_11);
        model.setName("Rani Juice");
        model.setPrice("from 90 RS");
        list.add(model);
        model = new ProductModel();
        model.setImage(R.drawable.drink_12);
        model.setName("Sprite");
        model.setPrice("from 50 RS");
        list.add(model);

        return list;
    }
}
