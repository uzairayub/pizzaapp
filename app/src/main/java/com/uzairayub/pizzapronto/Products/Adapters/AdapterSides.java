package com.uzairayub.pizzapronto.Products.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;

import java.util.ArrayList;

public class AdapterSides extends RecyclerView.Adapter<AdapterSides.MyViewHolder> {

    ArrayList<ProductModel> list;

    public AdapterSides(ArrayList<ProductModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pizza, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        holder.itemImage.setImageResource(list.get(position).getImage());
        holder.itemDescription.setText(list.get(position).getDescription());
        holder.itemName.setText(list.get(position).getName());
        holder.itemPrice.setText(list.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemDescription, itemName, itemPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.itemImage);
            itemDescription = itemView.findViewById(R.id.itemDescription);
            itemDescription.setVisibility(View.GONE);
            itemName = itemView.findViewById(R.id.itemName);
            itemPrice = itemView.findViewById(R.id.itemPrice);
        }
    }
}
