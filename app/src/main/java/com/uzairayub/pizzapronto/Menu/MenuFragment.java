package com.uzairayub.pizzapronto.Menu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.uzairayub.pizzapronto.MapsActivity;
import com.uzairayub.pizzapronto.Products.ProductModel;
import com.uzairayub.pizzapronto.R;
import com.uzairayub.pizzapronto.Utils.Constants;
import com.uzairayub.pizzapronto.Utils.SnappyDBUtil;
import com.uzairayub.pizzapronto.Utils.Utilities;

import java.util.ArrayList;

public class MenuFragment extends Fragment implements View.OnClickListener {

    private Button btnPlaceOrder;
    private View view;
    private TextView tvName, tvEmail;
    private RecyclerView rvCard;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_menu, container, false);
        initial();
        return view;
    }

    private void initial() {

        tvName = view.findViewById(R.id.tvName);
        tvEmail = view.findViewById(R.id.tvEmail);

        tvName.setText(Utilities.getInstance(getContext()).getStringPreferences(Constants.NAME));
        tvEmail.setText(Utilities.getInstance(getContext()).getStringPreferences(Constants.EMAIL));

        btnPlaceOrder = view.findViewById(R.id.btnPlaceOrder);
        btnPlaceOrder.setOnClickListener(this);
        rvCard = view.findViewById(R.id.rvCard);
        rvCard.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<ProductModel> list = SnappyDBUtil.getList(Constants.CART_LIST, ArrayList.class);
        if (list != null) {
            rvCard.setAdapter(new AdapterCart(list));
        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnPlaceOrder.getId()) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(getContext(), MapsActivity.class));
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }

        }
    }
}
